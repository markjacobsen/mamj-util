package org.mamj.util;

import org.apache.commons.codec.binary.Base64;

public class Convert {
    public static String toBase64(String val) {
		return new String(Base64.encodeBase64(val.getBytes()));
	}
}