#!/bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: commitPush.sh commitComment"
  exit 1
fi

commitComment=$1

git add -A
git commit -am "$commitComment"
git push origin master